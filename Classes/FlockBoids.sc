// // The Nature of Code
// // Daniel Shiffman
// // http://natureofcode.com
//
// // SC Implementation, Daniel Peterson, University of Washington, 2017 - 2021
//
// // particle class
// // Methods for Separation, Cohesion, Alignment added
//
// Flock {
// 	var <>particles, <>cohesion, <>separation, <>centerForce, <>centerLocation, startLocation, startVelocity; // An ArrayList for all the particles
// 	var <>maxspeed, <>maxforce, currentForce;
// 	var <>separationForce, <>alignmentForce, <>cohesionForce;
// 	var <flockEnvs, <flockCenterEnv, flockTimes, flockPositions, now, <envScale;
//
// 	*new { |particles, cohesion = 60.0, separation = 30.0, centerForce = 0.99, centerLocation = (Cartesian.new), startLocation = (Cartesian.new(1, 1, 0)), startVelocity = (Cartesian.new(0.1, -0.2, 0.1))|
// 		^super.newCopyArgs(particles, cohesion, separation, centerForce, centerLocation, startLocation, startVelocity).initFlock;
// 	}
//
// 	initFlock {
// 		// Start with initial food and creatures
// 		this.particles ?? {
// 			particles = 30.collect{arg i;
// 				Particle.new(startLocation, startVelocity * Cartesian.new(1.0.rand2, 1.0.rand2, 1.0.rand2))
// 			};
// 		};
// 		maxspeed = this.size.linlin(1, 30, 0.02, 2.0, nil);
// 		maxforce = this.size.linlin(1, 30, 0.01, 0.05, nil);
// 		cohesionForce = 1.5;
// 		separationForce = 1.0;
// 		alignmentForce = 1.0;
// 		flockPositions = Array.newClear(this.particles.size);
// 		flockTimes = [];
// 		now = 0.0;
// 	}
//
// 	run { |cForce, mForce, sep, coh, cLocation|
// 		cForce !? { this.centerForce_(cForce) };
// 		mForce !? { this.maxforce_(mForce) };
// 		sep !? { this.separation_(sep) };
// 		coh !? { this.cohesion_(coh) };
// 		cLocation !? { this.centerLocation_(cLocation) };
// 		this.particles.do{ |particle| this.update(particle)
// 		}
// 	}
//
//
// 	update { |particle|
// 		particle.velocity_(particle.velocity +
// 			(this.separate(particle, separation, maxspeed, maxforce) * separationForce) +
// 			(this.align(particle) * alignmentForce) +
// 			(this.cohere(particle) * cohesionForce) +
// 		this.seek(particle, centerLocation, (centerForce * particle.location.rho.linlin(0, 50, 0.0, 0.1, nil))));
// 		// ("particleLocation: " ++ particle.location).postln;
// 		// ("particleVelocity: " ++ particle.velocity).postln;
// 		// Limit speed
// 		particle.velocity_(particle.velocity.limit(maxspeed));
// 		particle.location_(particle.location + particle.velocity);
//
// 	}
//
// 	// A method that calculates and applies a steering force towards a target
// 	// STEER = DESIRED MINUS VELOCITY
// 	seek {arg particle, target, max;
// 		var desired, steer;
// 		desired = target - particle.location;  // A vector pointing from the location to the target
// 		// Normalize desired and scale to maximum speed
// 		desired = desired.normalize;
// 		desired = desired * maxspeed;
// 		// Steering = Desired minus Velocity
// 		steer = desired - particle.velocity;
// 		steer = steer.limit(max);  // Limit to maximum steering force
// 		^steer;
// 	}
//
// 	// Separation
// 	// Method checks for nearby particles and steers away
// 	separate {arg particle;
// 		var cart, count, distance, diff;
// 		cart = Cartesian.new;
// 		count = 0;
// 		// For every particle in the system, check if it's too close
// 		particles.do {arg otherparticle;
// 			distance = particle.location.dist(otherparticle.location);
// 			// If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
// 			((distance > 0) && (distance < separation)).if({
// 				// Calculate vector pointing away from neighbor
// 				diff = particle.location - otherparticle.location;
// 				diff = diff.normalize;
// 				diff = diff/distance;        // Weight by distance
// 				cart = cart + diff;
// 				count = count + 1;            // Keep track of how many
// 			})
// 		};
// 		// Average -- divide by how many
// 		(count > 0).if({
// 			cart = cart/count;
// 		});
//
// 		// As long as the vector is greater than 0
// 		(cart.rho > 0).if({
// 			// Implement Reynolds: Steering = Desired - Velocity
// 			cart = cart.normalize;
// 			cart = cart * maxspeed;
// 			cart = cart - particle.velocity;
// 			cart = cart.limit(maxforce);
// 		});
// 		^cart;
// 	}
//
// 	// Alignment
// 	// For every nearby particle in the system, calculate the average velocity
// 	align {arg particle;
// 		var cart, count, distance;
// 		cart = Cartesian.new;
// 		count = 0;
// 		particles.do {arg otherparticle;
// 			distance = particle.location.dist(otherparticle.location);
// 			((distance > 0) && (distance < cohesion)).if({
// 				cart = cart + otherparticle.velocity;
// 				count = count + 1;
// 			})
// 		};
// 		(count > 0).if({
// 			cart = cart/count;
// 			cart = cart.normalize;
// 			cart = cart * maxspeed;
// 			cart = cart - particle.velocity;
// 			cart = cart.limit(maxforce);
// 			}, {
// 				cart = Cartesian.new;
// 		})
// 		^cart
// 	}
//
// 	// Cohesion
// 	// For the average location (i.e. center) of all nearby particles, calculate steering vector towards that location
// 	cohere {arg particle;
// 		var cart, count, distance;
// 		cart = Cartesian.new;  // Start with empty vector to accumulate all locations
// 		count = 0;
// 		particles.do{arg otherparticle;
// 			distance = particle.location.dist(otherparticle.location);
// 			((distance > 0) && (distance < cohesion)).if({
// 				cart = cart + otherparticle.location; // Add location
// 				count = count + 1;
// 			})
// 		};
//
// 		(count > 0).if({
// 			cart = cart/count;
// 			cart = this.seek(particle, cart, maxforce);  // Steer towards the location
// 			}, {
// 				cart = Cartesian.new;
// 		});
// 		^cart
// 	}
//
// 	flockToEnvs { |flockSpeed, cForce, cLocation, scale, translate|
//
// 		while({now < flockSpeed.times.sum}, {
// 			this.run(cForce: cForce[now], cLocation: cLocation[now]);
// 			particles.do{ |particle, i|
// 				flockPositions[i] = flockPositions[i].add(particle.location)
// 			};
// 			flockTimes = flockTimes.add(flockSpeed[now]);
// 			now = now + (flockSpeed[now]);
// 		});
//
// 		flockEnvs = flockPositions.collect{ |position, i|
// 			CartesianEnv.new(position, flockTimes)
// 		};
//
// 		envScale = flockEnvs.collect{ |flockEnv|
// 			[flockEnv.x.levels.abs.maxItem, flockEnv.y.levels.abs.maxItem, flockEnv.z.levels.abs.maxItem].maxItem
// 		}.maxItem;
//
// 		scale.notNil.if({
// 			flockEnvs.do{ |flockEnv|
// 				scale.isKindOf(CartesianEnv).if({
// 					flockEnv.scale(envScale.reciprocal).blend(scale)
// 					}, {
// 						flockEnv.scale(envScale.reciprocal).scale(scale)
// 				})
// 			};
// 		});
//
// 		translate.notNil.if({
// 			flockEnvs.do{ |flockEnv|
// 				translate.isKindOf(CartesianEnv).if({
// 					flockEnv.translateEnv(translate)
// 					}, {
// 						flockEnv.translate(translate)
// 				})
// 			}
// 		});
//
// 		envScale = flockEnvs.collect{ |flockEnv|
// 			[flockEnv.x.levels.abs.maxItem, flockEnv.y.levels.abs.maxItem, flockEnv.z.levels.abs.maxItem].maxItem
// 		}.maxItem;
//
// 	}
//
// 	flockCenterToEnv { |flockSpeed, cForce, cLocation, scale|
// 		var size;
// 		flockCenterEnv = IdentityDictionary.new(know: true);
// 		flockEnvs ?? { this.flockToEnvs(flockSpeed, cForce, cLocation, scale) };
// 		size = flockEnvs.size;
// 		flockCenterEnv.x_(Env(flockEnvs.collect(_.x).collect(_.levels).sum/size, flockTimes));
// 		flockCenterEnv.y_(Env(flockEnvs.collect(_.y).collect(_.levels).sum/size, flockTimes));
// 		flockCenterEnv.z_(Env(flockEnvs.collect(_.z).collect(_.levels).sum/size, flockTimes));
// 	}
//
// 	plotCenter { |flockSpeed, cForce, cLocation, scale|
// 		var pointView, window, drawTask, now = 0.0, cart;
// 		cart = Cartesian.new;
// 		flockCenterEnv ?? { this.flockCenterToEnv(flockSpeed, cForce, cLocation, scale) };
// 		window = Window.new("Flock Center", Window.screenBounds);
// 		pointView = PointView.new(window, window.bounds).showIndices_(false).rotate_(0).tumble_(0.5pi).pointColors_(Color.black);
// 		drawTask = Task({
// 			loop{
// 				cart.x_(flockCenterEnv.x[now]);
// 				cart.y_(flockCenterEnv.y[now]);
// 				cart.z_(flockCenterEnv.z[now]);
// 				{pointView.points_([cart, Cartesian.new(envScale, envScale, envScale)])}.defer;
// 				now = now + 0.01;
// 				0.01.wait;
// 			}
// 		}).play;
// 		window.front;
// 		window.onClose_({ drawTask.stop });
// 	}
//
// 	plot { |flockSpeed, cForce, cLocation, scale|
// 		var pointView, window, drawTask, now = 0.0, carts;
// 		flockEnvs ?? { this.flockToEnvs(flockSpeed, cForce, cLocation, scale) };
// 		carts = flockEnvs.size.collect{ Cartesian.new } ++ Cartesian.new(envScale, envScale, envScale);
// 		window = Window.new("Flock Center", Window.screenBounds);
// 		pointView = PointView.new(window, window.bounds).showIndices_(false).rotate_(0).tumble_(0.5pi).pointColors_(Color.black);
// 		drawTask = Task({
// 			loop{
// 				flockEnvs.do{ |flockEnv, i|
// 					carts[i].x_(flockEnv.x[now]);
// 					carts[i].y_(flockEnv.y[now]);
// 					carts[i].z_(flockEnv.z[now])
// 				};
// 				{pointView.points_(carts)}.defer;
// 				now = now + 0.01;
// 				0.01.wait;
// 			}
// 		}).play;
// 		window.front;
// 		window.onClose_({ drawTask.stop });
// 	}
//
// }